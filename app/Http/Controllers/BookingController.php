<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\Client;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

class BookingController extends Controller
{
    /**
     * Get all bookings.
     *
     * @return Collection
     */
    public function index()
    {
        return Booking::all();
    }

    public function store(Request $request)
    {
        $bookInfo = $request->validate([
            'client.username' => 'required',
            'client.name' => 'required',
            'client.email' => 'required|email',
            'client.phone' => 'required',
            'price' => 'required',
            'check_in' => 'required',
            'check_out' => 'required'
        ]);

        $client = Client::firstOrCreate([
            'username' => $bookInfo['client']['username'],
            'name' => $bookInfo['client']['name'],
            'email' => $bookInfo['client']['email'],
            'phone' => $bookInfo['client']['username']
        ]);

        $booking = new Booking($bookInfo);
        $client->bookings()->save($booking);

        return response()->json($booking, 201);
    }
}
